
// Hide Header on on scroll down
let didScrollWindow;
let lastScrollWindowTop = 0;
function hasScrolled() {
    let st = $(this).scrollTop();
    if (st > lastScrollWindowTop && st > $('.header').outerHeight()){// Scroll Down
        $('.header').removeClass('header-up').addClass('header-down');
    } else {// Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('.header').removeClass('header-down').addClass('header-up');
        }
    }
    lastScrollWindowTop = st;
}

//function preloader
function loader() {
    $(window).on('load', function () {
        if($('.preloader')){
            $('.preloader').addClass('loaded');
            if ($('.preloader').hasClass('loaded')) {
                $('.preloader').delay(300).queue(function () {
                    $(this).remove();
                });
            }
        }
    });
}

//button like heart
function buttonLikeHeart(){
    $(this).toggleClass('liked');
}

//form validate
function emptyCheckValid(that) {
    let el = $(that);
    let flag = true;
    if (!el.val()){
        let label = $(that).parent().find('label').text();
        el.addClass('error').parent().find('.alert-error').text('Please enter ' + label).show();
        flag = false;
    }else {
        el.removeClass('error').parent().find('.alert-error').text('').hide();
        flag = true;
    }
    return flag;
}
function telCheckValid(that) {
    let el = $(that);
    let flag = emptyCheckValid(that);
    let phone_regex = /^[\+]?[(]?[\+]?[0-9]{2,4}[)]?[-\s\.]?[0-9]{2,4}[-\s\.]?[0-9]{1,6}$/im;
    if (flag && !phone_regex.test(el.val())){
        let label = $(that).parent().find('label').text();
        el.addClass('error').parent().find('.alert-error').text('Please enter the correct ' + label + ' format').show();
        flag = false;
    }
    return flag;
}
function checkFileUpload(that) {
    let flag = true;
    let formControl = $(that).parent();
    let alertError = $(that).parents('.form-group').find('.alert-error');

    //check type file
    let fileExtension = ['docx', 'doc', 'pdf', 'png', 'jpg', 'jpeg', 'webp', 'xls', 'xlsx', 'ppt', 'pps', 'pptx', 'ppsx'];
    let fileExtentionCurrent = $(that).val().split('.').pop().toLowerCase();
    if ($.inArray(fileExtentionCurrent, fileExtension) == -1) {
        formControl.addClass('error');
        alertError.text("Sorry, doesn't support this file format!").show();
        $(that).val('');
        flag = false;
        return flag;
    }

    //check size file
    if (flag){
        let reader = new FileReader();
        reader.onload = function (e) {
            let sizeFile = that.files[0].size;
            if ((sizeFile / 1024) > (2 * 1024)){
                formControl.addClass('error');
                alertError.text("Please upload a file smaller than 2MB!").show();
                $(that).val('');
                flag = false;
                return flag;
            }else {
                formControl.removeClass('error');
                alertError.text('').hide();
                flag = true;
                return flag;
            }
        };
        reader.readAsDataURL(that.files[0]);
    }
}
function textValid() {
    let textValid = $('.textValid');
    let flag = true;
    for (let i = 0; i < textValid.length; i++){
        let textValidThis = textValid[i];
        let checker = emptyCheckValid(textValidThis);
        if (!checker){
            flag = false
        }
    }
    return flag;
}
function submitFormVacancy(event){
    event.preventDefault();
    let checkTextValid = textValid();
    let checkTelValid = telCheckValid($('.telValid'));
    if (checkTextValid && checkTelValid){
        $(this)[0].submit();
    }
}


//=== main ==========================================//
$(function () {
    loader();

    //header toggle when scroll page
    $(window).scroll(function(event){
        didScrollWindow = true;
    });
    setInterval(function() {
        if (didScrollWindow) {
            hasScrolled();
            didScrollWindow = false;
        }
    }, 250);

    //header choose languge
    $('.header-info_language').on('click', function () {
        $(this).toggleClass('open');
        $('.header-info_language__box ul').slideToggle();
    });

    if (screen.width < 992){
        //header mmenu
        $('#header-nav-mobile').mmenu(
            {
                extensions: ['fx-menu-slide', 'shadow-page', 'shadow-panels', 'listview-large', 'pagedim-white'],
                iconPanels: true,
                counters: true,
                keyboardNavigation: {
                    enable: true,
                    enhance: true
                },
                searchfield: {
                    placeholder: 'Tìm kiếm...'
                },
                navbar: {
                    //title: 'Advanced menu'
                },
                navbars: [
                    {
                        position: 'top',
                        content: ['searchfield']
                    }, {
                        position: 'top',
                        content: ['breadcrumbs', 'close']
                    }, {
                        position: 'bottom',
                        content: ['<a href="https://ezs.vn" target="_blank">© 2020 - Design by J-Job</a>']
                    }
                ]
            },{
                searchfield: {
                    clear: true
                }
            }
        );
    }else {
        //theia sticky sidebar
        if ($('.theia-content').length && $('.theia-sidebar').length) {
            $('.theia-content, .theia-sidebar').theiaStickySidebar({
                additionalMarginTop: 100,
                additionalMarginBottom: 15
            });
        }
    }

    //select2
    if ($('.my-select-2').length){
        $('.my-select-2').select2();
    }

    //slick slider
    if ($('.home-special-areas_slick').length){
        $('.home-special-areas_slick').slick({
            infinite: true,
            dots: false,
            speed: 1000,
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            prevArrow: '<button type="button" class="slick_prev"><i class="fal fa-chevron-left"></i></button>',
            nextArrow: '<button type="button" class="slick_next"><i class="fal fa-chevron-right"></i></button>',
            responsive: [
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 575,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        }).removeClass('d-flex');
    }
    if ($('.home-jobs-opening_slick').length){
        $('.home-jobs-opening_slick').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
            dotsClass: 'slick_dots',
            autoplay: true,
            autoplaySpeed: 10000,
        });
    }
    if ($('.aboutus_services__slick').length){
        $('.aboutus_services__slick').slick({
            infinite: true,
            dots: false,
            speed: 1000,
            slidesToShow: 2,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            prevArrow: '<button type="button" class="slick_prev"><i class="fal fa-chevron-left"></i></button>',
            nextArrow: '<button type="button" class="slick_next"><i class="fal fa-chevron-right"></i></button>',
            responsive: [
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });
    }

    //background paralax js
    if($('.bg-paralax').length){
        $('.bg-paralax').parallax();
    }

    //button like heart
    $('.button-like-heart').on('click', buttonLikeHeart);

    //form Vacancy submit
    $(document).on('keyup', '.textValid', function () {
        emptyCheckValid(this);
    });
    $(document).on('keyup', '.telValid', function () {
        telCheckValid(this);
    });
    $(document).on('change', '.label-upload-file input[type="file"]', function () {
        checkFileUpload(this);
    });
    $(document).on('submit', '.form-vacancy-submit', submitFormVacancy);

});
